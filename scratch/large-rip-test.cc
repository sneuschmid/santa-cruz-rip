/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Spencer Neuschmid
 */

// Network topology
//
// The network topology is a medium sized network with 10 nodes each having a 
// distance metric of one to its neighboring nodes. 
//    SRC
//     |<=== source network
//
//
// All nodes are RIPv2 routers.
//
// Test setup:
//
// The test will set up the nodes and wait for the network to come to convergence. Once
// this occurs, pings will be sent out through the network. At 100 seconds into the simulation,
// multiple links will be cut and the network will be monitored to see how it recovered. 
// 
//
// If "showPings" is enabled, the user will see:
// 1) if the ping has been acknowledged
// 2) if a Destination Unreachable has been received by the sender
// 3) nothing, when the Echo Request has been received by the destination but
//    the Echo Reply is unable to reach the sender.
// Examining the .pcap files with Wireshark can confirm this effect.


#include <fstream>
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-routing-table-entry.h"

// Include helpful simulation functions
#define SIM_END_TIME (300)
#include "../scratch/rip_sim_helpers.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("RipSimpleRouting");


int main (int argc, char **argv)
{
  // Set a random seed for the simulation. 1 is default. 
  RngSeedManager::SetSeed(1); // Used default, 123, 539
	
  bool verbose = false;
  bool printRoutingTables = false;
  bool showPings = false;
  bool showConvergence = false;
  bool rip = false;
  std::string SplitHorizon ("PoisonReverse");

  // Specify command line inputs into the simulation. 
  CommandLine cmd;
  cmd.AddValue ("verbose", "turn on log components", verbose);
  cmd.AddValue ("printRoutingTables", "Print routing tables at 30, 60 and 90 seconds", printRoutingTables);
  cmd.AddValue ("showPings", "Show Ping6 reception", showPings);
  cmd.AddValue ("showConvergence", "Show when nodes reach convergence", showConvergence);
  cmd.AddValue ("rip", "Show RIP control messages", rip);
  cmd.AddValue ("splitHorizonStrategy", "Split Horizon strategy to use (NoSplitHorizon, SplitHorizon, PoisonReverse)", SplitHorizon);
  cmd.Parse (argc, argv);

  if (rip)
  {
	LogComponentEnable ("RipSimpleRouting", LOG_LEVEL_INFO);
    LogComponentEnable ("Rip", LOG_LEVEL_ALL);
  }

  if (verbose)
    {
      LogComponentEnableAll (LogLevel (LOG_PREFIX_TIME | LOG_PREFIX_NODE));
      LogComponentEnable ("RipSimpleRouting", LOG_LEVEL_INFO);
      LogComponentEnable ("Rip", LOG_LEVEL_ALL);
      LogComponentEnable ("Ipv4Interface", LOG_LEVEL_ALL);
      LogComponentEnable ("Icmpv4L4Protocol", LOG_LEVEL_ALL);
      LogComponentEnable ("Ipv4L3Protocol", LOG_LEVEL_ALL);
      LogComponentEnable ("ArpCache", LOG_LEVEL_ALL);
      LogComponentEnable ("V4Ping", LOG_LEVEL_ALL);
    }

  if (SplitHorizon == "NoSplitHorizon")
    {
      Config::SetDefault ("ns3::Rip::SplitHorizon", EnumValue (RipNg::NO_SPLIT_HORIZON));
    }
  else if (SplitHorizon == "SplitHorizon")
    {
      Config::SetDefault ("ns3::Rip::SplitHorizon", EnumValue (RipNg::SPLIT_HORIZON));
    }
  else
    {
      Config::SetDefault ("ns3::Rip::SplitHorizon", EnumValue (RipNg::POISON_REVERSE));
    }
	
 # if 1 // Create routers
  NS_LOG_INFO ("Create routers.");
  Ptr<Node> a = CreateObject<Node> ();
  Names::Add ("RouterA", a);
  Ptr<Node> b = CreateObject<Node> ();
  Names::Add ("RouterB", b);
  Ptr<Node> c = CreateObject<Node> ();
  Names::Add ("RouterC", c);
  Ptr<Node> d = CreateObject<Node> ();
  Names::Add ("RouterD", d);
  Ptr<Node> e = CreateObject<Node> ();
  Names::Add ("RouterE", e);
  Ptr<Node> f = CreateObject<Node> ();
  Names::Add ("RouterF", f);
  Ptr<Node> g = CreateObject<Node> ();
  Names::Add ("RouterG", g);
  Ptr<Node> h = CreateObject<Node> ();
  Names::Add ("RouterH", h);
  Ptr<Node> i = CreateObject<Node> ();
  Names::Add ("RouterI", i);
  Ptr<Node> j = CreateObject<Node> ();
  Names::Add ("RouterJ", j);
  Ptr<Node> k = CreateObject<Node> ();
  Names::Add ("RouterK", k);
  Ptr<Node> l = CreateObject<Node> ();
  Names::Add ("Routerl", l);
  Ptr<Node> m = CreateObject<Node> ();
  Names::Add ("Routerm", m);
  Ptr<Node> n = CreateObject<Node> ();
  Names::Add ("Routern", n);
  Ptr<Node> o = CreateObject<Node> ();
  Names::Add ("Routero", o);
  Ptr<Node> p = CreateObject<Node> ();
  Names::Add ("Routerp", p);
  Ptr<Node> q = CreateObject<Node> ();
  Names::Add ("Routerq", q);
  Ptr<Node> r = CreateObject<Node> ();
  Names::Add ("Routerr", r);
  Ptr<Node> s = CreateObject<Node> ();
  Names::Add ("Routers", s);
  Ptr<Node> t = CreateObject<Node> ();
  Names::Add ("Routert", t);
  Ptr<Node> u = CreateObject<Node> ();
  Names::Add ("Routeru", u);
  Ptr<Node> v = CreateObject<Node> ();
  Names::Add ("Routerv", v);
  Ptr<Node> w = CreateObject<Node> ();
  Names::Add ("Routerw", w);
  Ptr<Node> x = CreateObject<Node> ();
  Names::Add ("Routerx", x);
  Ptr<Node> y = CreateObject<Node> ();
  Names::Add ("Routery", y);
  Ptr<Node> z = CreateObject<Node> ();
  Names::Add ("Routerz", z);
  Ptr<Node> zz = CreateObject<Node> ();
  Names::Add ("Routerzz", zz);
  Ptr<Node> zzz = CreateObject<Node> ();
  Names::Add ("Routerzzz", zzz);
  Ptr<Node> zzzz = CreateObject<Node> ();
  Names::Add ("Routerzzzz", zzzz);
  #endif
# if 1 // Create hosts
  NS_LOG_INFO ("Create hosts.");
  Ptr<Node> host1 = CreateObject<Node> ();
  Names::Add ("HostNode1", host1);
  Ptr<Node> host2 = CreateObject<Node> ();
  Names::Add ("HostNode2", host2);
  Ptr<Node> host3 = CreateObject<Node> ();
  Names::Add ("HostNode3", host3);
  Ptr<Node> host4 = CreateObject<Node> ();
  Names::Add ("HostNode4", host4);
  Ptr<Node> host5 = CreateObject<Node> ();
  Names::Add ("HostNode5", host5);
  Ptr<Node> host6 = CreateObject<Node> ();
  Names::Add ("HostNode6", host6);
  Ptr<Node> host7 = CreateObject<Node> ();
  Names::Add ("HostNode7", host7);
  Ptr<Node> host8 = CreateObject<Node> ();
  Names::Add ("HostNode8", host8);
#endif

#if 1  // Link hosts to routers
  NodeContainer net0 (host1, a);
  NodeContainer net20 (host2, e);
  NodeContainer net9 (host3, y);
  NodeContainer net14 (host4, zzz);
  NodeContainer net39 (host5, r);
  NodeContainer net34 (host6, o);
  NodeContainer net37 (host7, l);
  NodeContainer net38 (host8, i);
#endif
  
 #if 1 // Link routers together
  NodeContainer net1 (a, b);
  
  NodeContainer net2 (b, c);
  
  NodeContainer net3 (c, x);
  NodeContainer net6 (c, w);

  NodeContainer net4 (x, y);
  NodeContainer net5 (x, w);
  
  NodeContainer net8 (w, v);
 
  NodeContainer net17 (v, d);
  NodeContainer net16 (v, u);
  NodeContainer net7 (v, y);
  
  NodeContainer net10 (y, z);
  
  NodeContainer net11 (zzzz, z);
  NodeContainer net12 (zzzz, zz);
  
  NodeContainer net15 (zz, u);
  NodeContainer net13 (zz, zzz);
  
  NodeContainer net18 (d, e);
  
  NodeContainer net19 (u, e);
  NodeContainer net22 (u, t);
  
  NodeContainer net23 (f, g);
  NodeContainer net21 (f, t);
  
  NodeContainer net24 (q, g);
  NodeContainer net25 (q, r);
  
  NodeContainer net26 (s, r);
  NodeContainer net40 (s, p);
  
  NodeContainer net27 (h, p);
  NodeContainer net28 (h, i);
  
  NodeContainer net29 (o, i);
  NodeContainer net31 (o, n);
  
  NodeContainer net30 (n, j);
  
  NodeContainer net32 (k, j);
  NodeContainer net36 (k, l);
  NodeContainer net33 (k, m);
  
  NodeContainer net35 (m, n);

#endif

  // Define routers
  NodeContainer routers1 (a, b, c, d);
  NodeContainer routers2 (e, f, g, h);
  NodeContainer routers3 (i, j, k, l);
  NodeContainer routers4 (m, n, o, p);
  NodeContainer routers5 (q, r, s, t);
  NodeContainer routers6 (u, v, w, x);
  NodeContainer routers7 (y, z, zz, zzz);
  NodeContainer routers8 (zzzz);
  NodeContainer interRout1 (routers1, routers2, routers3, routers4);
  NodeContainer interRout2 (routers5, routers6, routers7, routers8);
  NodeContainer routers(interRout1, interRout2);
  // Define Hosts
  NodeContainer hosts1 (host1, host2, host3, host4);
  NodeContainer hosts2 (host5, host6, host7, host8);
  NodeContainer nodes (hosts1, hosts2);

#if 1  // Create link layer channels.
  NS_LOG_INFO ("Create channels.");
  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", DataRateValue (5000000));
  csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));
  NetDeviceContainer ndc0 = csma.Install (net0);
  NetDeviceContainer ndc1 = csma.Install (net1);
  NetDeviceContainer ndc2 = csma.Install (net2);
  NetDeviceContainer ndc3 = csma.Install (net3);
  NetDeviceContainer ndc4 = csma.Install (net4);
  NetDeviceContainer ndc5 = csma.Install (net5);
  NetDeviceContainer ndc6 = csma.Install (net6);
  NetDeviceContainer ndc7 = csma.Install (net7);
  NetDeviceContainer ndc8 = csma.Install (net8);
  NetDeviceContainer ndc9 = csma.Install (net9);
  NetDeviceContainer ndc10 = csma.Install (net10);
  NetDeviceContainer ndc11 = csma.Install (net11);
  NetDeviceContainer ndc12 = csma.Install (net12);
  NetDeviceContainer ndc13 = csma.Install (net13);
  NetDeviceContainer ndc14 = csma.Install (net14);
  NetDeviceContainer ndc15 = csma.Install (net15);
  NetDeviceContainer ndc16 = csma.Install (net16);
  NetDeviceContainer ndc17 = csma.Install (net17);
  NetDeviceContainer ndc18 = csma.Install (net18);
  NetDeviceContainer ndc19 = csma.Install (net19);
  NetDeviceContainer ndc20 = csma.Install (net20);
  NetDeviceContainer ndc21 = csma.Install (net21);
  
  NetDeviceContainer ndc22 = csma.Install (net22);
  NetDeviceContainer ndc23 = csma.Install (net23);
  NetDeviceContainer ndc24 = csma.Install (net24);
  NetDeviceContainer ndc25 = csma.Install (net25);
  NetDeviceContainer ndc26 = csma.Install (net26);
  NetDeviceContainer ndc27 = csma.Install (net27);
  NetDeviceContainer ndc28 = csma.Install (net28);
  NetDeviceContainer ndc29 = csma.Install (net29);
  NetDeviceContainer ndc30 = csma.Install (net30);
  NetDeviceContainer ndc31 = csma.Install (net31);
  NetDeviceContainer ndc32 = csma.Install (net32);
  NetDeviceContainer ndc33 = csma.Install (net33);
  NetDeviceContainer ndc34 = csma.Install (net34);
  NetDeviceContainer ndc35 = csma.Install (net35);
  NetDeviceContainer ndc36 = csma.Install (net36);
  NetDeviceContainer ndc37 = csma.Install (net37);
  NetDeviceContainer ndc38 = csma.Install (net38);
  NetDeviceContainer ndc39 = csma.Install (net39);
  NetDeviceContainer ndc40 = csma.Install (net40);
#endif

  NS_LOG_INFO ("Create IPv4 and routing");
  RipHelper ripRouting;

  Ipv4ListRoutingHelper listRH;
  listRH.Add (ripRouting, 0);

  InternetStackHelper internet;
  internet.SetIpv6StackInstall (false);
  internet.SetRoutingHelper (listRH);
  // internet.Install (routers1);
  // internet.Install (routers2);
  // internet.Install (routers3);
  // internet.Install (routers4);
  // internet.Install (routers5);
  // internet.Install (routers6);
  // internet.Install (routers7);
  // internet.Install (routers8);
  internet.Install(routers);

  InternetStackHelper internetNodes;
  internetNodes.SetIpv6StackInstall (false);
  internetNodes.Install (nodes);

#if 1  // Assign addresses.
  // The source and destination networks have global addresses
  // The "core" network just needs link-local addresses for routing.
  // We assign global addresses to the routers as well to receive
  // ICMPv6 errors.
  NS_LOG_INFO ("Assign IPv4 Addresses.");
  Ipv4AddressHelper ipv4;

  ipv4.SetBase (Ipv4Address ("10.0.0.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic0 = ipv4.Assign (ndc0);

  ipv4.SetBase (Ipv4Address ("10.0.1.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic1 = ipv4.Assign (ndc1);

  ipv4.SetBase (Ipv4Address ("10.0.2.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic2 = ipv4.Assign (ndc2);

  ipv4.SetBase (Ipv4Address ("10.0.3.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic3 = ipv4.Assign (ndc3);

  ipv4.SetBase (Ipv4Address ("10.0.4.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic4 = ipv4.Assign (ndc4);

  ipv4.SetBase (Ipv4Address ("10.0.5.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic5 = ipv4.Assign (ndc5);

  ipv4.SetBase (Ipv4Address ("10.0.6.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic6 = ipv4.Assign (ndc6);

  ipv4.SetBase (Ipv4Address ("10.0.7.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic7 = ipv4.Assign (ndc7);

  ipv4.SetBase (Ipv4Address ("10.0.8.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic8 = ipv4.Assign (ndc8);

  ipv4.SetBase (Ipv4Address ("10.0.9.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic9 = ipv4.Assign (ndc9);

  ipv4.SetBase (Ipv4Address ("10.0.10.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic10 = ipv4.Assign (ndc10);

  ipv4.SetBase (Ipv4Address ("10.0.11.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic11 = ipv4.Assign (ndc11);

  ipv4.SetBase (Ipv4Address ("10.0.12.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic12 = ipv4.Assign (ndc12);

  ipv4.SetBase (Ipv4Address ("10.0.13.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic13 = ipv4.Assign (ndc13);

  ipv4.SetBase (Ipv4Address ("10.0.14.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic14 = ipv4.Assign (ndc14);

  ipv4.SetBase (Ipv4Address ("10.0.15.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic15 = ipv4.Assign (ndc15);

  ipv4.SetBase (Ipv4Address ("10.0.16.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic16 = ipv4.Assign (ndc16);

  ipv4.SetBase (Ipv4Address ("10.0.17.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic17 = ipv4.Assign (ndc17);

  ipv4.SetBase (Ipv4Address ("10.0.18.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic18 = ipv4.Assign (ndc18);

  ipv4.SetBase (Ipv4Address ("10.0.19.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic19 = ipv4.Assign (ndc19);

  ipv4.SetBase (Ipv4Address ("10.0.20.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic20 = ipv4.Assign (ndc20);

  ipv4.SetBase (Ipv4Address ("10.0.21.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic21 = ipv4.Assign (ndc21);

  ipv4.SetBase (Ipv4Address ("10.0.22.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic22 = ipv4.Assign (ndc22);

  ipv4.SetBase (Ipv4Address ("10.0.23.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic23 = ipv4.Assign (ndc23);

  ipv4.SetBase (Ipv4Address ("10.0.24.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic24 = ipv4.Assign (ndc24);

  ipv4.SetBase (Ipv4Address ("10.0.25.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic25 = ipv4.Assign (ndc25);

  ipv4.SetBase (Ipv4Address ("10.0.26.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic26 = ipv4.Assign (ndc26);

  ipv4.SetBase (Ipv4Address ("10.0.27.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic27 = ipv4.Assign (ndc27);

  ipv4.SetBase (Ipv4Address ("10.0.28.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic28 = ipv4.Assign (ndc28);

  ipv4.SetBase (Ipv4Address ("10.0.29.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic29 = ipv4.Assign (ndc29);

  ipv4.SetBase (Ipv4Address ("10.0.30.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic30 = ipv4.Assign (ndc30);

  ipv4.SetBase (Ipv4Address ("10.0.31.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic31 = ipv4.Assign (ndc31);

  ipv4.SetBase (Ipv4Address ("10.0.32.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic32 = ipv4.Assign (ndc32);

  ipv4.SetBase (Ipv4Address ("10.0.33.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic33 = ipv4.Assign (ndc33);

  ipv4.SetBase (Ipv4Address ("10.0.34.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic34 = ipv4.Assign (ndc34);

  ipv4.SetBase (Ipv4Address ("10.0.35.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic35 = ipv4.Assign (ndc35);

  ipv4.SetBase (Ipv4Address ("10.0.36.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic36 = ipv4.Assign (ndc36);

  ipv4.SetBase (Ipv4Address ("10.0.37.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic37 = ipv4.Assign (ndc37);

  ipv4.SetBase (Ipv4Address ("10.0.38.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic38 = ipv4.Assign (ndc38);

  ipv4.SetBase (Ipv4Address ("10.0.39.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic39 = ipv4.Assign (ndc39);

  ipv4.SetBase (Ipv4Address ("10.0.40.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic40 = ipv4.Assign (ndc40);
#endif

  Ptr<Ipv4StaticRouting> staticRouting;
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host1->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.0.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host2->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.20.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host3->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.9.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host4->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.14.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host5->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.39.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host6->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.34.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host7->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.37.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host8->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.38.2", 1 );

  // Optionally, print out the routing tables at specified times to help debugging.
  if (printRoutingTables)
    {
      RipHelper routingHelper;

      Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (&std::cout);

	  double firstTime = 10;
	  double secondTime = 100;
	  double thirdTime = 200;
	  double fourthTime = 300;
	  double finalTime = 500;

      uint8_t numRouters = 11;
	  Ptr<Node> routersToPrint[numRouters] = {a, b, c, d, e, f, g, h, i, j, k};
	
	  PrintRouteTables(firstTime, routersToPrint, numRouters, routingStream);
	  PrintRouteTables(secondTime, routersToPrint, numRouters, routingStream);
	  PrintRouteTables(thirdTime, routersToPrint, numRouters, routingStream);
	  PrintRouteTables(fourthTime, routersToPrint, numRouters, routingStream);
	  PrintRouteTables(finalTime, routersToPrint, numRouters, routingStream);

   }

  // Create the ping applications that will simulate application traffic.
  NS_LOG_INFO ("Create Applications.");
  uint32_t packetSize = 1024;
  Time interPacketInterval = Seconds (1.0);

  char pingDest[] = "10.0.20.1";
  PingSetup(pingDest, host1, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  char pingDest2[] = "10.0.9.1";
  PingSetup(pingDest2, host1, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  char pingDest3[] = "10.0.14.1";
  PingSetup(pingDest3, host1, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  char pingDest4[] = "10.0.9.1";
  PingSetup(pingDest4, host4, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  char pingDest5[] = "10.0.35.1";
  PingSetup(pingDest5, host7, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  char pingDest6[] = "10.0.37.1";
  PingSetup(pingDest6, host6, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  char pingDest7[] = "10.0.29.1";
  PingSetup(pingDest7, host2, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  char pingDest8[] = "10.0.14.1";
  PingSetup(pingDest8, host8, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  char pingDest9[] = "10.0.29.1";
  PingSetup(pingDest9, host4, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  char pingDest10[] = "10.0.9.1";
  PingSetup(pingDest10, host8, packetSize, interPacketInterval, showPings, Seconds (1.0), Seconds (201.0));

  AsciiTraceHelper ascii;
  csma.EnableAsciiAll (ascii.CreateFileStream ("test_output/rip-large-routing.tr"));
  csma.EnablePcapAll ("test_output/rip-large-routing", true);

  // Tear down and bring up links at regular intervals.
  //Simulator::Schedule (Seconds (40), &TearDownLink, b, d, 3, 2);
  //Simulator::Schedule (Seconds (40), &TearDownLink, c, d, 3, 1);
  //Simulator::Schedule (Seconds (61), &BringUpLink, b, d, 3, 2);
  
  // Print out the overhead statistics at the end of the simulation.
  Simulator::Schedule (Seconds(SIM_END_TIME), &PrintRipStats, routers);
  
  // If the user asks for convergence, then print out each time a router reaches convergence.
  if (showConvergence)
  {
	CheckConvergence(routers);
  }
  
  // Now, do the actual simulation.
  NS_LOG_INFO ("Run Simulation.");
  Simulator::Stop (Seconds (SIM_END_TIME));
  Simulator::Run ();
  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");
}

