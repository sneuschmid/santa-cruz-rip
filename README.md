
The Network Simulator, Version 3
================================

## Table of Contents:

1) [Santa Cruz RIP Overview]
2) [Building NS-3]
3) [Running NS-3]

Note:  Much more substantial information about ns-3 can be found at
http://www.nsnam.org

## Santa Cruz RIP Overview

Santa Cruz RIP is a modified version of RIP that is being theorized and implemented in the CCRG,
Professor JJ Garcia-Luna-Aceves' lab at UCSC. This implementation is being worked on by Spencer Neuschmid.
The main goal of this project is to make a loop-free RIP, and potentially improve the performance of RIP
as well. The main performance metrics that are cared about in these simulations are time to convergence, 
signaling overhead, end-to-end latency, packet delivery ratio, and most importantly loop-freedom. 

This implementation is created using the NS-3 framework. sers not familiar with NS-3 should go read the 
tutorials before attempting to understand the specific implementation details. Refere to the following sections 
to build and run the code. 

## Building NS-3

This section is taken driectly from the source code found at http://www.nsnam.org. For further details on building
NS-3, please refer to the NS-3 tutorial: https://www.nsnam.org/docs/release/3.30/tutorial/html/getting-started.html.

The code for the framework and the default models provided
by ns-3 is built as a set of libraries. User simulations
are expected to be written as simple programs that make
use of these ns-3 libraries.

To build the set of default libraries and the example
programs included in this package, you need to use the
tool 'waf'. Detailed information on how to use waf is
included in the file doc/build.txt

However, the real quick and dirty way to get started is to
type the command
```shell
./waf configure --enable-examples
```

followed by

```shell
./waf
```

in the directory which contains this README file. The files
built will be copied in the build/ directory.

The current codebase is expected to build and run on the
set of platforms listed in the [release notes](RELEASE_NOTES)
file.

Other platforms may or may not work: we welcome patches to
improve the portability of the code to these other platforms.

## Running Santa Cruz RIP in NS-3

Sample simulations can be found in  the scratch directory that is at the same level of this README file. 

It should be noted that if you want to run these simulations with vanilla RIPv2, then you should work off of 
the master branch, but if you want to test Santa Cruz RIP, then you must pull and run off of the 
add_ref_dist_to_rip developement branch. 

The tests in this folder have various simulations that test different features and aspects of Santa Cruz RIP.
There are many commanalities between them, such as how to print various performance metrics. 

To run each script, use the following command where [program] is swapped with one of the simulation names
and the [--options] can be replaced by many of the options mentioned below.

```shell
./waf --run 'scratch/[program] [--options]'
```

The diferent programs are as follows:

rip-test: Run a simple topology of 4 nodes with 2 hosts on either end of the topology. At 40 seconds, a link is cut 
between the two hosts. At 60 seconds the link is brought back up. This program runs for 300 seconds total. The reason
for this simulation is to see how RIP and Santa Cruz RIP react to link failures. Take out the line of code that brings the 
link back up to see how RIP responds after a path timeout occurs.

medium-rip-test: Run a topology of 11 nodes with 5 hosts scattered throughout the network. The test simply runs for 300 
seconds without any link changes, although lines of code are commented out that would take down and bring down links
as the user would see fit. The main reson for ths test i to measure how fast convergence can be reached in a spread out
topology and how the nodes signal each other during that time. 

loop-rip-test: This simulation is similar to the rip-test. This simulation will specifically attempt to cause a routing loop, and it 
does in the original RIP. Santa Cruz RIP handles this possibility and prevents the loop from occurring. 

simple-rip: This simulation only has 2 nodes with a host attached to each. This is a good place to start for someone trying to
understand how Santa Cruz RIP works and how to modify simulations in NS-3.

The different options are as follows:

--showPings: This will show the application traffic in the topology. It will also give RTT latency and th packet loss rate.

--showConvergence: This will print out when a node thinks it has reached convergence. Convergence in these simulations is 
defined by no activity or changes occurring in the nodes routing table for some quiet time, in this case 30 seconds. RIP is
a distance vector routing protocol and show never technically know when it has reached convergence. 

--printRoutingTables: Print the routing tables of each node at certain points in the simulation. 

There are additional options that change the way NS-3 behaves, but these are the main ones that are useful for monitoring the 
performance of RIP. 

